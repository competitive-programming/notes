# Books
  * [Algorithm toolbox](http://e-maxx.ru/bookz/files/algorithms_toolbox_mehlhorn.pdf) - `Mehlhorn`
  * [BrockCSC](https://github.com/BrockCSC/acm-icpc/wiki) - Wiki for ACM ICPC
  * [DP](https://www.iarcs.org.in/inoi/online-study-material/topics/dp-basics.php) - CMI
